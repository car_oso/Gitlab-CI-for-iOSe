//
//  Gitlab_CI_for_iOSApp.swift
//  Gitlab-CI-for-iOS
//
//  Created by Carlos Ortíz on 08/10/21.
//

import SwiftUI

@main
struct Gitlab_CI_for_iOSApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
