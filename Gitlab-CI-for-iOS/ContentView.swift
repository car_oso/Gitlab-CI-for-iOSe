//
//  ContentView.swift
//  Gitlab-CI-for-iOS
//
//  Created by Carlos Ortíz on 08/10/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, Flink!, Hello Carlos")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
